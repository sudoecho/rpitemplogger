--------------------
RpiTemplogger Readme
--------------------

01/20/13

Templogger consists of a shell script which generates a log and a python script which analyzes the generated log and gives a report with statistics relating to the core temperature of the Raspberry Pi.

This is a very rough piece of code at the moment. Written, and only tested, on Raspbian Wheezy.

Usage:

Place the shell script in a directory e.g. /usr/bin and add edit your crontab (crontab -e) to run it as often as you want.

E.G. To have it run every half an hour add the following line to the bottom on your crontab:

0,30 * * * * /usr/bin/templogger.sh

The script will add a line with the current Time, Date and Temperature (in the format of HH-MM-SS:MM-DD-YY:TEMP) to the file tmplog every time it is run.

Now run the templogoutput.py file to generate a report based on the data contained in the log file.

chmod + x tempoutput.py
./tempoutput.py

The script will create a report and output it to the file defined in the variable outfile (currently defaults /home/pi/log/rpitemp, to be changed soon)
Cat the contents of the rpitemp to view the report
cat /home/pi/log/rpitemp

Currently displays Latest Temperature Record, Previous Temperature, Difference between Latest/Previous, Highest Recorded Temperature, Lowest Recorded Temperature and Average Temperature. 

OPTIONS:

-f, 	--fahrenheit
		Output the temperatures in Fahrenheit

-o,		--output 
		Output report to terminal

-w,		--web 
		Generate an html report file (default rpitemp.html)

-l,		--long
		Output entire log
 
